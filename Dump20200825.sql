-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: cps
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `addon_offer`
--

DROP TABLE IF EXISTS `addon_offer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `addon_offer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  `business` tinyint(1) DEFAULT NULL,
  `sc` text,
  `tp` text,
  `name` varchar(55) DEFAULT NULL,
  `description` text,
  `bscs_provisioning` text,
  `prepaid_provisioning` text,
  `pcrf_provisioning` text,
  `generic` text,
  `active` text,
  `nesto` text,
  `n1` text,
  `n2` text,
  `n3` text,
  `n4` text,
  `n5` text,
  `n6` text,
  `n7` text,
  `n8` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addon_offer`
--

LOCK TABLES `addon_offer` WRITE;
/*!40000 ALTER TABLE `addon_offer` DISABLE KEYS */;
INSERT INTO `addon_offer` VALUES (1,'post',0,'',',12,','mega','mega internet','[{\"servicename\":\"ime_servisa\"}]','kjsdk','lkdmfkm','dskmflksdmf','dsmfdm','dsmfsdmf',',dsmfsd,m','.dmfs.sd,m','.,dsmf.smd,','.d,mfsd,m',',.dsmvf.sdm','m,cvm.,m','d,.mfv.m','cv ,ms'),(2,'postpaid',0,'',',222,445,','200 MB Interneta','Internet','{[\"nesto\":\"n\"]}','akaskka','lsakd;laskd;l','lsmf;lsd','1','asd','dsklfkdj','dslkfjdskf;l','dfdslkfn','dsnd','vndvk','kdvn','dmfndskn','dvs'),(3,'pripaid',1,',122,','','Minuti','Minuti dodatak','bscs','prepaid-provisioning','pcrf','gene','0','nesto','jos nesto','blabla','hahahha','aaa','fkdf','ksdfk','dksflj','dsklfj'),(4,'postpaid',0,'','','','','','','','','','','','','','','','','',''),(5,'postpaid',0,'',',222,445,','git101','Description','','','','generic_name','1','','','','','','','','',''),(6,'pripaid',1,',989,','','Roming','Roming','','','','','1','nnn','','','','','','','',''),(7,'postpaid',1,'',',ALL,-655,','Roming 100','Roming 100','bsc',NULL,'pcrf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,'pripaid',1,',151,',NULL,'Roming MB','Roming MB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,'postpaid',0,NULL,',141,','Net 1GB','Net 1GB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,'post',0,NULL,NULL,'Net 2GB','Net 2GB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,'pripaid',0,NULL,NULL,'Net 100MB','Net 100Mb',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `addon_offer` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-25 11:59:43
