package rs.tele.gui.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import rs.tele.gui.entity.AddonOffer;
import rs.tele.gui.service.AddonOfferService;

@Controller
public class AddonOfferController {

	private final AddonOfferService addonOfferService;

	@Autowired
	public AddonOfferController(final AddonOfferService theAddonOfferService) {
		addonOfferService = theAddonOfferService;
	}

	@GetMapping("/")
	public String viewHomePage(final Model model) {

		final String keyword = null;
		return listByPage(model, 1, "id", "desc", keyword);
	}

	@GetMapping("/page/{pageNumber}")
	public String listByPage(final Model model, @PathVariable("pageNumber") final int currentPage,
			@Param("sortField") final String sortField, @Param("sortDir") final String sortDir,
			@Param("keyword") final String keyword) {

		final Page<AddonOffer> page = addonOfferService.listAll(currentPage, sortField, sortDir, keyword);
		final long totalItems = page.getTotalElements();
		final int totalPages = page.getTotalPages();

		final List<AddonOffer> listAddonOffers = page.getContent();

		model.addAttribute("currentPage", currentPage);
		model.addAttribute("totalItems", totalItems);
		model.addAttribute("totalPages", totalPages);
		model.addAttribute("listAddonOffers", listAddonOffers);
		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDir", sortDir);
		model.addAttribute("keyword", keyword);

		final String reverseSortDir = sortDir.equals("asc") ? "desc" : "asc";
		model.addAttribute("reverseSortDir", reverseSortDir);

		return "index";

	}

	@GetMapping("/showNewAddonOfferForm")
	public String showNewAddonOfferForm(final Model model) {
		final AddonOffer addonOffer = new AddonOffer();
		model.addAttribute("addonOffer", addonOffer);

		return "new_addonOffer";
	}

	@PostMapping("/saveAddonOffer")
	public String saveAddonOffer(@ModelAttribute("addonOffer") final AddonOffer addonOffer) {
		addonOfferService.save(addonOffer);
		return "redirect:/";
	}

	@GetMapping("/showFormForUpdate/{id}")
	public ModelAndView showFormForUpdate(@PathVariable(value = "id") final int id) {
		final ModelAndView mav = new ModelAndView("update_addonOffer");

		final AddonOffer addonOffer = addonOfferService.findById(id);

		mav.addObject("addonOffer", addonOffer);
		return mav;
	}

}
