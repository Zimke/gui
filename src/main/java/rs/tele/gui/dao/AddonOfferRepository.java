package rs.tele.gui.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import rs.tele.gui.entity.AddonOffer;

@Repository
public interface AddonOfferRepository extends JpaRepository<AddonOffer, Integer> {

	@Query("SELECT a FROM AddonOffer a WHERE a.name LIKE %?1%" + " OR a.nesto LIKE %?1%")
	public Page<AddonOffer> findAll(String keyword, Pageable pageable);
}
