package rs.tele.gui.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "addon_offer")
public class AddonOffer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "type")
	private String type;

	@Column(name = "business")
	private boolean business;

	private String sc;

	private String tp;

	private String name;
	private String description;
	private String bscs_provisioning;
	private String prepaid_provisioning;
	private String pcrf_provisioning;
	private String generic;
	private String active;
	private String nesto;
	private String n1;
	private String n2;
	private String n3;
	private String n4;
	private String n5;
	private String n6;
	private String n7;
	private String n8;

	public AddonOffer() {
	}

	public int getId() {
		return id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public boolean isBusiness() {
		return business;
	}

	public void setBusiness(final boolean business) {
		this.business = business;
	}

	public String getSc() {
		return sc;
	}

	public void setSc(final String sc) {
		this.sc = sc;
	}

	public String getTp() {
		return tp;
	}

	public void setTp(final String tp) {
		this.tp = tp;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getBscs_provisioning() {
		return bscs_provisioning;
	}

	public void setBscs_provisioning(final String bscs_provisioning) {
		this.bscs_provisioning = bscs_provisioning;
	}

	public String getPrepaid_provisioning() {
		return prepaid_provisioning;
	}

	public void setPrepaid_provisioning(final String prepaid_provisioning) {
		this.prepaid_provisioning = prepaid_provisioning;
	}

	public String getPcrf_provisioning() {
		return pcrf_provisioning;
	}

	public void setPcrf_provisioning(final String pcrf_provisioning) {
		this.pcrf_provisioning = pcrf_provisioning;
	}

	public String getGeneric() {
		return generic;
	}

	public void setGeneric(final String generic) {
		this.generic = generic;
	}

	public String getActive() {
		return active;
	}

	public void setActive(final String active) {
		this.active = active;
	}

	public String getNesto() {
		return nesto;
	}

	public void setNesto(final String nesto) {
		this.nesto = nesto;
	}

	public String getN1() {
		return n1;
	}

	public void setN1(final String n1) {
		this.n1 = n1;
	}

	public String getN2() {
		return n2;
	}

	public void setN2(final String n2) {
		this.n2 = n2;
	}

	public String getN3() {
		return n3;
	}

	public void setN3(final String n3) {
		this.n3 = n3;
	}

	public String getN4() {
		return n4;
	}

	public void setN4(final String n4) {
		this.n4 = n4;
	}

	public String getN5() {
		return n5;
	}

	public void setN5(final String n5) {
		this.n5 = n5;
	}

	public String getN6() {
		return n6;
	}

	public void setN6(final String n6) {
		this.n6 = n6;
	}

	public String getN7() {
		return n7;
	}

	public void setN7(final String n7) {
		this.n7 = n7;
	}

	public String getN8() {
		return n8;
	}

	public void setN8(final String n8) {
		this.n8 = n8;
	}

	@Override
	public String toString() {
		return "AddonOffer [id=" + id + ", type=" + type + ", business=" + business + ", sc=" + sc + ", tp=" + tp
				+ ", name=" + name + ", description=" + description + ", bscs_provisioning=" + bscs_provisioning
				+ ", prepaid_provisioning=" + prepaid_provisioning + ", pcrf_provisioning=" + pcrf_provisioning
				+ ", generic=" + generic + ", active=" + active + ", nesto=" + nesto + ", n1=" + n1 + ", n2=" + n2
				+ ", n3=" + n3 + ", n4=" + n4 + ", n5=" + n5 + ", n6=" + n6 + ", n7=" + n7 + ", n8=" + n8 + "]";
	}

}
