package rs.tele.gui.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import rs.tele.gui.dao.AddonOfferRepository;
import rs.tele.gui.entity.AddonOffer;

@Service
public class AddonOfferServiceImpl implements AddonOfferService {

	private final AddonOfferRepository addonOfferRepository;

	@Autowired
	public AddonOfferServiceImpl(final AddonOfferRepository theAddonOfferRepository) {
		this.addonOfferRepository = theAddonOfferRepository;
	}

	@Override
	public AddonOffer findById(final int theId) {
		final Optional<AddonOffer> result = this.addonOfferRepository.findById(theId);

		AddonOffer theAddonOffer = null;

		if (result.isPresent()) {
			theAddonOffer = result.get();
		} else {
			// we didn't find the AddonOffer
			throw new RuntimeException("Did not find AddonOffer id - " + theId);
		}

		return theAddonOffer;
	}

	@Override
	public void save(final AddonOffer theAddonOffer) {
		this.addonOfferRepository.save(theAddonOffer);
	}

	@Override
	public Page<AddonOffer> listAll(final int pageNumber, final String sortField, final String sortDir,
			final String keyword) {
		Sort sort = Sort.by(sortField);
		sort = sortDir.equals("asc") ? sort.ascending() : sort.descending();

		final Pageable pageable = PageRequest.of(pageNumber - 1, 5, sort);
		if (keyword != null) {
			return addonOfferRepository.findAll(keyword, pageable);
		}

		return addonOfferRepository.findAll(pageable);
	}

	@Override
	public List<AddonOffer> findAll() {
		return addonOfferRepository.findAll();
	}

}
