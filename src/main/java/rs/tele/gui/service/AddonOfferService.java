package rs.tele.gui.service;

import java.util.List;

import org.springframework.data.domain.Page;

import rs.tele.gui.entity.AddonOffer;

public interface AddonOfferService {

	public List<AddonOffer> findAll();

	public AddonOffer findById(int theId);

	public void save(AddonOffer theAddonOffer);

	Page<AddonOffer> listAll(int pageNumber, String sortField, String sortDir, String keyword);

}
